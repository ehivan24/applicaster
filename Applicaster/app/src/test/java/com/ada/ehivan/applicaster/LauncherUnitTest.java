package com.ada.ehivan.applicaster;

import android.app.Activity;
import android.content.Context;
import android.database.Cursor;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Build;
import android.widget.Button;
import android.widget.ListView;

import com.ada.ehivan.applicaster.database.DatabasePreserveData;
import com.ada.ehivan.applicaster.twitterengine.SearchTweet;
import com.ada.ehivan.applicaster.utils.UtilsClass;
import com.twitter.sdk.android.Twitter;
import com.twitter.sdk.android.core.TwitterAuthConfig;
import com.twitter.sdk.android.tweetui.SearchTimeline;
import com.twitter.sdk.android.tweetui.TweetTimelineListAdapter;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.robolectric.Robolectric;
import org.robolectric.RobolectricTestRunner;
import org.robolectric.annotation.Config;
import org.robolectric.shadows.ShadowLog;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Locale;

import io.fabric.sdk.android.Fabric;

import static com.ada.ehivan.applicaster.constants.Constants.TWITTER_KEY;
import static com.ada.ehivan.applicaster.constants.Constants.TWITTER_SECRET;
import static org.hamcrest.Matchers.equalTo;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertThat;
import static org.junit.Assert.assertTrue;

/**
 * setup for Robolectric
 * ./gradlew test --continue --info
 */
@RunWith(RobolectricTestRunner.class)
@Config(constants = BuildConfig.class, sdk = Build.VERSION_CODES.LOLLIPOP)
public class LauncherUnitTest {

    private static final String SEARCH_FOR_HASH_TAG = "Applicaster";


    /**
     * Application Context
     */
    private Context mContext;
    /**
     * Current Activity Launcher.class
     */
    private Activity launcherActivity;
    /**
     * Current Activity SearchTweet.class
     */
    private Activity displayTweetsActivity;

    /**
     * Sets the launcherActivity
     * @throws Exception
     */
    @Before
    public void setUp() throws Exception{
        launcherActivity = Robolectric.setupActivity(Launcher.class);
        displayTweetsActivity = Robolectric.setupActivity(SearchTweet.class);
        ShadowLog.stream = System.out;
    }

    /**
     * Checks for the internet Connection function
     *
     * @throws Exception
     */
    @Test
    public void isConnectedToInternetTest() throws Exception{

        boolean isConnected = UtilsClass.isConnectingToInternet(launcherActivity.getApplicationContext());
        boolean isNetworkConnectd = false;
        ConnectivityManager connectivity = (ConnectivityManager)
                launcherActivity.getApplicationContext().getSystemService(Context.CONNECTIVITY_SERVICE);
        if (connectivity != null) {
            NetworkInfo networkInfo = connectivity.getActiveNetworkInfo();
            if(networkInfo != null && networkInfo.isConnected()){

                isNetworkConnectd = true;

            }
        }

        assertEquals("Checks for internet Connection Function", isConnected, isNetworkConnectd);

    }

    /**
     * isConnectingToInternet(...) returns a non null object
     * @throws Exception
     */
    @Test
    public void isConnectedToNetworkReturnsANonNullValueTest() throws Exception{
        boolean isConnected = UtilsClass.isConnectingToInternet(launcherActivity.getApplicationContext());
        assertNotNull(isConnected);
    }

    /**
     * checks for TimeStamp function
     * @throws Exception
     */
    @Test
    public void getTimeStampTest() throws Exception{
        String currentTimeStampFromFunction = UtilsClass.getCurrentTimeStamp();
        String currentTimeStampFromSystem = null;

        Timestamp timestamp = new Timestamp(System.currentTimeMillis());
        currentTimeStampFromSystem = timestamp.toString();

        assertEquals("Current Time Stamp Should be equals",
                currentTimeStampFromFunction, currentTimeStampFromSystem);

    }

    /**
     * R.id.btn_search_status_or_hashtag text should display "TWEET"
     * @throws Exception
     */
    @Test
    public void clickingButtonTextShouldEqualToTest() throws Exception {

        Button mainButton = (Button) launcherActivity.findViewById(R.id.btn_search_status_or_hashtag);

        assertTrue(mainButton.isClickable());

        mainButton.performClick();

        assertThat(mainButton.getText().toString(), equalTo("TWEET"));
    }


    /**
     * Test if rows can be inserted in Local DataBase
     * @throws Exception
     */
    @Test
    public void dbShouldBeAbleToStoreDataTest() throws Exception {

        int numRowsInDB = 0;

        //Deletes the Current Local Database
        boolean deleteDb = launcherActivity.deleteDatabase(DatabasePreserveData.DATABASE_NAME);

        assertTrue(deleteDb);

        //Creates a bew Database
        DatabasePreserveData databasePreserveData =
                new DatabasePreserveData(launcherActivity.getApplicationContext());

        //inserts a new Row To DB
        if (databasePreserveData.insertSearch(SEARCH_FOR_HASH_TAG, UtilsClass.getCurrentTimeStamp())) {
            numRowsInDB = databasePreserveData.numberOfRows();
        }

        assertEquals("Database should contain one row ", 1, numRowsInDB);
    }


    /**
     * This Function Testes that the code can search for a given word in Database
     * @throws Exception
     */
    @Test
    public void getFieldSavedOnDatabaseTest() throws Exception{
        String dbKeyidWord = "";
        DatabasePreserveData databasePreserveData =
                new DatabasePreserveData(launcherActivity.getApplicationContext());

        if (databasePreserveData.insertSearch(SEARCH_FOR_HASH_TAG, UtilsClass.getCurrentTimeStamp())) {

            Cursor cursor = databasePreserveData.getSearchForHashtagSaved(SEARCH_FOR_HASH_TAG);

            if (cursor.moveToFirst()) {

                dbKeyidWord = cursor.getString(
                        cursor.getColumnIndex(DatabasePreserveData.PREV_SEARCHES_COLUMN_DATA));
            }
        }

        assertEquals("Applicaster", dbKeyidWord);
    }


    /**
     * Test for the Array List that is returned from the Database.
     * @throws Exception
     */
    @Test
    public void getAllKeyWordsFromDBTEst() throws Exception{
        DatabasePreserveData databasePreserveData =
                new DatabasePreserveData(launcherActivity.getApplicationContext());

        ArrayList<String> getAllData = databasePreserveData.getAllPreviousSearches();

        assertNotNull(getAllData);
        assertEquals("Size Of the Array", 0, getAllData.size());

        ShadowLog.d("TEST", String.valueOf(getAllData.size()));

        if (databasePreserveData.insertSearch(SEARCH_FOR_HASH_TAG, UtilsClass.getCurrentTimeStamp())) {
            ArrayList<String> getAllDataAfterInsert = databasePreserveData.getAllPreviousSearches();
            assertNotNull(getAllData);
            assertEquals("Size Of the Array", 1, getAllDataAfterInsert.size());

        } else {

            assertEquals("Size Of the Array", 0, getAllData.size());
        }
    }

    /**
     * Test if removing a given row is executed correctly
     * @throws Exception
     */
    @Test
    public void canRowBeRemovedFromDBTest() throws Exception{
        int numRowsInDB, numOfRowsAfterInsert;
        DatabasePreserveData databasePreserveData =
                new DatabasePreserveData(launcherActivity.getApplicationContext());

        numRowsInDB = databasePreserveData.numberOfRows();
        assertEquals(0, numRowsInDB);

        ShadowLog.d("TEST", String.valueOf(numRowsInDB));

        if (databasePreserveData.insertSearch(SEARCH_FOR_HASH_TAG, UtilsClass.getCurrentTimeStamp())) {
            numOfRowsAfterInsert = databasePreserveData.numberOfRows();
            ShadowLog.d("TEST", String.valueOf(numOfRowsAfterInsert));
            assertEquals(1, numOfRowsAfterInsert);

            databasePreserveData.deleteRow(1);

            assertEquals(databasePreserveData.numberOfRows(), 0);
            ShadowLog.d("TEST", String.valueOf(databasePreserveData.numberOfRows()));

        } else {
            assertEquals(0, numRowsInDB);
        }
    }

    /**
     * Checks that the ListView is populated wih the Twitter API
     * @throws Exception
     */
    @Test
    public void setUpTimeLineFromTwitterEngineTest() throws Exception{
        TweetTimelineListAdapter timelineAdapter;
        SearchTimeline searchTimeline = new SearchTimeline.Builder().query("CNN")
                .maxItemsPerRequest(10)
                .languageCode(Locale.ENGLISH.getLanguage())
                .build();

        timelineAdapter = new TweetTimelineListAdapter.Builder(launcherActivity.getApplicationContext())
                .setTimeline(searchTimeline)
                .build();

        ListView listViewTimeLine = (ListView) displayTweetsActivity.findViewById(R.id.tweet_display_list_view);
        listViewTimeLine.setAdapter(timelineAdapter);

        assertTrue(listViewTimeLine.isClickable());

        assertEquals(listViewTimeLine.getAdapter(), timelineAdapter);

    }


    /***
     * Checks for the number of child generated after the connection
     * @throws Exception
     */
    @Test
    public void checkIfListViewHasChildsTest() throws Exception{
        TwitterAuthConfig authConfig = new TwitterAuthConfig(TWITTER_KEY, TWITTER_SECRET);
        Fabric.with(launcherActivity.getApplicationContext(), new Twitter(authConfig));
        launcherActivity.setContentView(R.layout.activity_search_tweet);

        ShadowLog.d("TEST", String.valueOf(authConfig.getRequestCode()));

        TweetTimelineListAdapter timelineAdapter;
        SearchTimeline searchTimeline = new SearchTimeline.Builder().query("CNN")
                .maxItemsPerRequest(10)
                .languageCode(Locale.ENGLISH.getLanguage())
                .build();

        timelineAdapter = new TweetTimelineListAdapter.Builder(launcherActivity.getApplicationContext())
                .setTimeline(searchTimeline)
                .build();

        ListView listViewTimeLine = (ListView) displayTweetsActivity.findViewById(R.id.tweet_display_list_view);
        listViewTimeLine.setEmptyView(displayTweetsActivity.findViewById(R.id.tweet_display_list_view));

        listViewTimeLine.setAdapter(timelineAdapter);

        int childCount = listViewTimeLine.getChildCount();
        ShadowLog.d("TEST", String.valueOf(childCount));

    }

    @After
    public void exit() throws Exception{

    }
}