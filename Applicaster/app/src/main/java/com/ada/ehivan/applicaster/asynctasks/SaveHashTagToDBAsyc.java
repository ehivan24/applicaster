package com.ada.ehivan.applicaster.asynctasks;

import android.app.Activity;
import android.content.Context;
import android.os.AsyncTask;
import android.util.Log;
import android.widget.Toast;

import com.ada.ehivan.applicaster.database.DatabasePreserveData;

/**
 * Saves The Previous search into the database.
 */
public class SaveHashTagToDBAsyc extends AsyncTask<String, Void, Void> {

    private Context mContext;

    /**
     * Contructor
     * @param context current context
     */
    public SaveHashTagToDBAsyc(Context context){
        if (context != null) {
            mContext = context;
        } else {
            Log.d("#HASHTAG", "Null Context");
        }
    }

    @Override
    protected Void doInBackground(String... strings) {
        final Activity mActivity = (Activity) mContext;
        DatabasePreserveData databasePreserveData = new DatabasePreserveData(mContext);
        final String mKeyword = strings[0];
        final String mTimeStamp = strings[1];

        if (mKeyword.length() > 0 && mTimeStamp != null){

            try {
                if (databasePreserveData.insertSearch(mKeyword, mTimeStamp)) {

                    mActivity.runOnUiThread(new Runnable() {
                        public void run() {

                            Toast.makeText(mActivity.getApplicationContext(),
                                    "Saved to DataBase " + mKeyword, Toast.LENGTH_SHORT).show();
                        }
                    });

                    Log.d("#HASHTAG", "Inserted");

                } else {
                    Log.d("#HASHTAG", "Failed To Insert Data to DB");
                }
            }catch (Exception e){

                Log.d("#HASHTAG", String.valueOf(e));
            }
        }
        return null;
    }
}
