package com.ada.ehivan.applicaster;

import android.content.Intent;
import android.database.Cursor;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.Toast;

import com.ada.ehivan.applicaster.asynctasks.SaveHashTagToDBAsyc;
import com.ada.ehivan.applicaster.constants.Constants;
import com.ada.ehivan.applicaster.database.DatabasePreserveData;
import com.ada.ehivan.applicaster.twitterengine.SearchTweet;
import com.ada.ehivan.applicaster.utils.UtilsClass;

import java.util.ArrayList;

/**
 *  Main Activity that contains a Edit TextBox and a Button.
 */
public class Launcher extends AppCompatActivity implements Constants {

    private AutoCompleteTextView searchHashtagEditText;
    private Button searchBtn;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_launcher);
        searchHashtagEditText = (AutoCompleteTextView)
                findViewById(R.id.search_hashtag_edit__text_view);

        searchBtn = (Button) findViewById(R.id.btn_search_status_or_hashtag);

        getAllHashTagsSaved();
    }


    /**
     * Checks whether the given hashtag exists in Database,
     * if the hashtag exits it wont be saved again.
     *
     * @param searchForHashTagEditText a given Hashtag to save into DB
     */
    private void saveThisKeyWordToDB(String searchForHashTagEditText) {
        String currentTimeStamp = UtilsClass.getCurrentTimeStamp();
        DatabasePreserveData databasePreserveData = new DatabasePreserveData(this);
        Cursor cursor = databasePreserveData.getSearchForHashtagSaved(searchForHashTagEditText);
        String dbKeyidWord = null;
        try {
            if (cursor.moveToFirst()) {

                dbKeyidWord = cursor.getString(
                        cursor.getColumnIndex(DatabasePreserveData.PREV_SEARCHES_COLUMN_DATA));
            }

        } catch (Exception e){
            Log.d("#HASHTAG", String.valueOf(e));
        }

        Log.d("#HASHTAG", "LOOK_UP " + dbKeyidWord );

        if (dbKeyidWord == null) {

            new SaveHashTagToDBAsyc(this).execute(searchForHashTagEditText, currentTimeStamp);
        }
    }

    /**
     * Retrieves the Hashtags or tweets previously searched from the Local Database.
     */
    private void getAllHashTagsSaved(){
        DatabasePreserveData databasePreserveData = new DatabasePreserveData(this);

        ArrayList<String> getAllData = databasePreserveData.getAllPreviousSearches();
        if (getAllData != null) {
            for (String str : getAllData) {
                Log.d("#HASHTAG", str);
            }

            ArrayAdapter adapter = new
                    ArrayAdapter(this, android.R.layout.simple_list_item_1, getAllData);

            searchHashtagEditText.setAdapter(adapter);
        }
    }


    /**
     * Checks whether the device is connected a network, wifi or cellular.
     * Then it gets the text from the Edit text box, if the length of the string is greater than
     * 0 chars, it will launch the next activity. The information passed to the next activity is the
     * #hashtag or tweet to search for.
     *
     * Moreover, it saves the previous search to the Database.
     *
     * @param view Button
     */
    public void searchTweetActivity(View view) {
        if(UtilsClass.isConnectingToInternet(this)) {
            String searchForHashTagEditText = searchHashtagEditText.getText().toString();

            // In order to launch the next activity the length of the text from the
            // EditText must be greater than 0

            if (searchForHashTagEditText.length() > 0) {

                Intent i = new Intent(this, SearchTweet.class);
                i.putExtra(SEARCH_FOR_HASHTAG, searchForHashTagEditText);

                saveThisKeyWordToDB(searchForHashTagEditText);

                startActivity(i);
                //clear Edit text Box
                searchHashtagEditText.setText("");
                finish();
            } else {

                Toast.makeText(this, "Enter a #hashtag or Tweet", Toast.LENGTH_LONG).show();
            }

        } else {
            Toast.makeText(this, "Device Not Connected To Internet", Toast.LENGTH_LONG).show();
            Log.d("#HASHTAG", "Device Not Connected To Internet");

        }
    }
}
