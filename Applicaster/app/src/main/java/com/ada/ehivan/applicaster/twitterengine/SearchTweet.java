package com.ada.ehivan.applicaster.twitterengine;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.widget.ListView;

import com.ada.ehivan.applicaster.Launcher;
import com.ada.ehivan.applicaster.R;
import com.ada.ehivan.applicaster.constants.Constants;
import com.twitter.sdk.android.Twitter;
import com.twitter.sdk.android.core.Callback;
import com.twitter.sdk.android.core.Result;
import com.twitter.sdk.android.core.TwitterAuthConfig;
import com.twitter.sdk.android.core.TwitterException;
import com.twitter.sdk.android.core.models.Tweet;
import com.twitter.sdk.android.tweetui.SearchTimeline;
import com.twitter.sdk.android.tweetui.TimelineResult;
import com.twitter.sdk.android.tweetui.TweetTimelineListAdapter;

import java.util.Locale;
import java.util.Timer;
import java.util.TimerTask;

import io.fabric.sdk.android.Fabric;

public class SearchTweet extends AppCompatActivity implements Constants {

    /**
     * SwipeRefresh layout
     */
    private SwipeRefreshLayout swipeLayout;
    /**
     * Tweeter API
     */
    private TweetTimelineListAdapter timelineAdapter;
    /**
     * ListView to store and display the tweets
     */
    private ListView timelineView;
    /**
     * Timer to refresh the apps
     */
    private Timer mTimer;


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        TwitterAuthConfig authConfig = new TwitterAuthConfig(TWITTER_KEY, TWITTER_SECRET);
        Fabric.with(this, new Twitter(authConfig));
        setContentView(R.layout.activity_search_tweet);

        swipeLayout = (SwipeRefreshLayout) findViewById(R.id.swipe_to_refresh_list_view);
        swipeLayout.setRefreshing(false);

        timelineView = (ListView) findViewById(R.id.tweet_display_list_view);

        Bundle intent = getIntent().getExtras();

        if(intent != null) {
            String performHashTagLookUP = intent.getString(SEARCH_FOR_HASHTAG);

            if (intent.containsKey(SEARCH_FOR_HASHTAG)) {
                setUpTimeLine(performHashTagLookUP, MAX_NUMBER_OF_ITEMS_PER_REQUEST);
                Log.d("#HASHTAG", performHashTagLookUP);
            }
        }

        refreshListAGivenMillis();
    }

    /**
     * Searches for a given hashtag or tweet from Tweeter.
     *
     * @param searchQuery either a hashtag or tweet for fetch from server.
     * @param maxNumberofItems max number of items per request.
     */
    private void setUpTimeLine(String searchQuery, int maxNumberofItems){
        if (!searchQuery.equals("") && maxNumberofItems > 1) {

            SearchTimeline searchTimeline = new SearchTimeline.Builder().query(searchQuery)
                    .maxItemsPerRequest(maxNumberofItems)
                    .languageCode(Locale.ENGLISH.getLanguage())
                    .build();

            // fetches data from the server.
            timelineAdapter = new TweetTimelineListAdapter.Builder(this)
                    .setTimeline(searchTimeline)
                    .build();

            timelineView.setEmptyView(findViewById(R.id.tweet_display_list_view));
            timelineView.setAdapter(timelineAdapter);

            swipeLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
                @Override
                public void onRefresh() {
                    refreshTwitterTimeLine();
                }
            });
        }
    }


    /**
     *  Callbacks from the server. If the request is successful the app will refresh and new data
     *  will be displayed; otherwise, an error will be log.
     */
    private void refreshTwitterTimeLine(){
        timelineAdapter.refresh(new Callback<TimelineResult<Tweet>>() {
            @Override
            public void success(Result<TimelineResult<Tweet>> result) {
                swipeLayout.setRefreshing(false);
            }

            @Override
            public void failure(TwitterException exception) {
                Log.d("#HASHTAG", "Failed to Refresh Tweeter Timeline");
            }
        });
    }

    /**
     * Requests the server for new data every number of milliseconds given
     * #REFRESH_APP_DATA_MILLI_SECONDS
     */
    private void refreshListAGivenMillis(){
        mTimer = new Timer();
        mTimer.schedule(new TimerTask() {
            @Override
            public void run() {
                refreshTwitterTimeLine();
                Log.d("#HASHTAG", "exec");
            }
        }, INITIAL_REFRESH_APP_DATA_MILLI_SECONDS_DELAY, REFRESH_APP_DATA_MILLI_SECONDS);
    }


    @Override
    protected void onDestroy() {
        super.onDestroy();

        if(mTimer != null){
            mTimer.cancel();
        }
    }

    @Override
    protected void onPause() {
        super.onPause();

        if(mTimer != null){
            mTimer.cancel();
        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();

        if(mTimer != null){
            mTimer.cancel();
        }

        startActivity(new Intent(this, Launcher.class));
        finish();
    }

    @Override
    protected void onResume() {
        super.onResume();
        if(mTimer == null) {
            refreshListAGivenMillis();
        }
    }
}