package com.ada.ehivan.applicaster.constants;


public interface Constants {

    // Tweeter keys
    public static final String TWITTER_KEY = "gwxWLc3FhlP6UapwiWAMvIl3p";
    public static final String TWITTER_SECRET = "1PXuukhrY1cuESEhl445TLlGXXh16jbXYDi1i10VG7szGIZAZ6";

    // String name for the intent
    public final static String SEARCH_FOR_HASHTAG =
            "com.ada.ehivan.applicaster.twitterengine.hash_tag_look_up";

    // max number of items per request.
    public final static int MAX_NUMBER_OF_ITEMS_PER_REQUEST = 10;

    // How the Timer Task waits before the start executing the task
    public final static int REFRESH_APP_DATA_MILLI_SECONDS = 5000;

    // How long the to wait before the app refreshes.
    public final static int INITIAL_REFRESH_APP_DATA_MILLI_SECONDS_DELAY = 5000;

}
