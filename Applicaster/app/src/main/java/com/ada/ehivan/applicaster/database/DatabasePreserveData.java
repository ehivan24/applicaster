package com.ada.ehivan.applicaster.database;


import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.DatabaseUtils;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import java.util.ArrayList;

/**
 * Database Helper that searches for the previous searches made by user
 */
public class DatabasePreserveData extends SQLiteOpenHelper {

    public static final String DATABASE_NAME = "previous_searches.db";
    public static final String PREVIOUS_SEARCHES_TABLE_NAME = "prev_tweets";
    public static final String PREV_SEARCHES_COLUMN_ID = "id";
    public static final String PREV_SEARCHES_COLUMN_DATA = "key_word";
    public static final String PREV_SEARCHES_COLUMN_TIMESTAMP = "time_tamp";

    /**
     * Constructor
     * @param context current Context
     */
    public DatabasePreserveData(Context context) {
        super(context, DATABASE_NAME , null, 1);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL(
                "create table  " + PREVIOUS_SEARCHES_TABLE_NAME +
                        " (" + PREV_SEARCHES_COLUMN_ID +  " integer primary key, " +
                            PREV_SEARCHES_COLUMN_DATA + " text, " +
                            PREV_SEARCHES_COLUMN_TIMESTAMP + " text)");
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL("DROP TABLE IF EXISTS " + PREVIOUS_SEARCHES_TABLE_NAME);
        onCreate(db);
    }

    /**
     * Isertes a newly searched hashtag or tweet
     * @param keyWord either a hashtag or tweet
     * @param timeStamp current time Stamp
     * @return returns a boolean indicating whether the data has been saved.
     */
    public boolean insertSearch(String keyWord, String timeStamp) {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues contentValues = new ContentValues();
        contentValues.put(PREV_SEARCHES_COLUMN_DATA, keyWord);
        contentValues.put(PREV_SEARCHES_COLUMN_TIMESTAMP, timeStamp);
        db.insert(PREVIOUS_SEARCHES_TABLE_NAME, null, contentValues);
        return true;
    }

    /**
     * Returns a cursor object with the given keyword
     * @param keyword a given keyword.
     * @return cursor obj
     */
    public Cursor getSearchForHashtagSaved(String keyword) {
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor =  db.rawQuery( "select  " + PREV_SEARCHES_COLUMN_DATA + " from "  +
                            PREVIOUS_SEARCHES_TABLE_NAME
                        + " where " + PREV_SEARCHES_COLUMN_DATA + "= ? ", new String[]{keyword} );
        return cursor;
    }

    /**
     * Gets the total number of rows in the database
     *
     * @return total number of rows in the database
     */
    public int numberOfRows(){
        SQLiteDatabase db = this.getReadableDatabase();
        int numRows = (int) DatabaseUtils.queryNumEntries(db, PREVIOUS_SEARCHES_TABLE_NAME);
        return numRows;
    }

    /**
     * Updates a Row on the database
     *
     * @param id Row current position
     * @param keyWord either a hashtag or tweet
     * @param timeStamp current time Stamp
     * @return returns a boolean indicating whether the data has been updated.
     */

    public boolean updateRow(Integer id, String keyWord, String timeStamp) {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues contentValues = new ContentValues();
        contentValues.put(PREV_SEARCHES_COLUMN_DATA, keyWord);
        contentValues.put(PREV_SEARCHES_COLUMN_TIMESTAMP, timeStamp);
        db.update(PREVIOUS_SEARCHES_TABLE_NAME, contentValues,
                "id = ? ", new String[] { Integer.toString(id) } );
        return true;
    }

    /**
     * Deletes a Row
     *
     * @param id row to be deleted.
     * @return Integer Object with the deleted row
     */
    public Integer deleteRow(Integer id) {
        SQLiteDatabase db = this.getWritableDatabase();
        return db.delete(PREVIOUS_SEARCHES_TABLE_NAME,
                "id = ? ",
                new String[] { Integer.toString(id) });
    }

    /**
     * Returns an ArrayList with all the rows in the databse
     *
     * @return ArrayList that contains all the database
     */
    public ArrayList<String> getAllPreviousSearches() {
        ArrayList<String> previousSearchesList = new ArrayList<>();

        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor =  db.rawQuery( "select * from " + PREVIOUS_SEARCHES_TABLE_NAME, null );
        cursor.moveToFirst();

        while(!cursor.isAfterLast()){
            previousSearchesList.add(
                    cursor.getString(cursor.getColumnIndex(PREV_SEARCHES_COLUMN_DATA)));
            cursor.moveToNext();
        }

        cursor.close();
        return previousSearchesList;
    }
}
