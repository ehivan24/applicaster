package com.ada.ehivan.applicaster.utils;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.util.Log;

import java.sql.Timestamp;

/**
 * This class checks for internet connection,
 * if there application cannot be connected to the internet
 * it will log/display an error to the user.
 *
 */
public class UtilsClass {

    //Avoid possible instantiation
    private UtilsClass(Context context){}

    /**
     * Returns a boolean state whether the device is connected to internet.
     *
     * @param context activity context
     * @return boolean if there is network connection
     */
    public static boolean isConnectingToInternet(Context context){
        boolean retVal = false;

        ConnectivityManager connectivity = (ConnectivityManager)
                context.getSystemService(Context.CONNECTIVITY_SERVICE);
        if (connectivity != null) {
            NetworkInfo networkInfo = connectivity.getActiveNetworkInfo();
            if(networkInfo != null && networkInfo.isConnected()){

                retVal = true;

            } else {

                retVal = false;
            }
        }
        Log.d("#HASHTAG", "Connected To Network? " + String.valueOf(retVal));
        return retVal;
    }


    /**
     * Retrieved the current timestamp
     * @return string that presumably is the current timestamp
     */
    public static String getCurrentTimeStamp(){
        String retVal = "";
        Timestamp timestamp = new Timestamp(System.currentTimeMillis());
        retVal = timestamp.toString();
        Log.d("#HASHTAG", retVal);
        return retVal;
    }
}
