package com.ada.ehivan.applicaster;

import android.content.Context;
import android.support.test.InstrumentationRegistry;
import android.support.test.rule.ActivityTestRule;
import android.support.test.runner.AndroidJUnit4;
import android.widget.Toast;

import com.ada.ehivan.applicaster.database.DatabasePreserveData;
import com.ada.ehivan.applicaster.utils.UtilsClass;

import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;

import static android.support.test.espresso.Espresso.onView;
import static android.support.test.espresso.action.ViewActions.clearText;
import static android.support.test.espresso.action.ViewActions.click;
import static android.support.test.espresso.action.ViewActions.closeSoftKeyboard;
import static android.support.test.espresso.action.ViewActions.typeText;
import static android.support.test.espresso.assertion.ViewAssertions.matches;
import static android.support.test.espresso.matcher.RootMatchers.withDecorView;
import static android.support.test.espresso.matcher.ViewMatchers.isDisplayed;
import static android.support.test.espresso.matcher.ViewMatchers.withHint;
import static android.support.test.espresso.matcher.ViewMatchers.withId;
import static android.support.test.espresso.matcher.ViewMatchers.withText;
import static org.hamcrest.Matchers.is;
import static org.hamcrest.core.IsNot.not;
import static org.junit.Assert.assertEquals;


/**
 * UI Testing performed with Espresso.
 */
@RunWith(AndroidJUnit4.class)
public class LauncherTest {

    private static final String SEARCH_FOR_HASH_TAG = "Applicaster";
    private static final String INITIAL_TEXT = "Search for #hashtag";
    private static final String INITIAL_TEXT_BUTTON = "TWEET";

    private static final long MAKE_A_PAUSE_BEFORE_EACH_TEST = 1000;

    /**
     * Activities Rule
     */
    @Rule
    public ActivityTestRule<Launcher> mActivityRule =
            new ActivityTestRule<>(Launcher.class);

    @Test
    public void useAppContext() throws Exception {
        Context appContext = InstrumentationRegistry.getTargetContext();
        assertEquals("com.ada.ehivan.applicaster", appContext.getPackageName());
    }

    /**
     * Checks that the Button with Id R.id.btn_search_status_or_hashtag
     * has the text "TWEET"
     * @throws Exception
     */
    @Test
    public void checksForButtonText() throws Exception{
        onView(withId(R.id.btn_search_status_or_hashtag)).check(matches(withText(INITIAL_TEXT_BUTTON)));
    }
    /**
     * Testes the class com.ada.ehivan.applicaster.Launcher
     * - EditText is Editable
     * - Text Can be entered in the EditText
     * - Button (R.id.btn_search_status_or_hashtag) is clickable
     * - Starts a new Activity
     * @throws Exception any exception that occurs while running the test
     */
    @Test
    public void enterTextOnEditBox() throws Throwable {

        DatabasePreserveData databasePreserveData =
                new DatabasePreserveData(mActivityRule.getActivity().getApplicationContext());

        makeAPause(MAKE_A_PAUSE_BEFORE_EACH_TEST);

        if (databasePreserveData.insertSearch("Applicaster", UtilsClass.getCurrentTimeStamp())) {

            mActivityRule.runOnUiThread(new Runnable() {
                public void run() {

                    Toast.makeText(mActivityRule.getActivity().getApplicationContext(),
                            "Applicaster Saved on DB", Toast.LENGTH_SHORT).show();
                }
            });
        }

        makeAPause(MAKE_A_PAUSE_BEFORE_EACH_TEST);

        onView(withId(R.id.search_hashtag_edit__text_view))
                .perform(typeText(INITIAL_TEXT), closeSoftKeyboard());

        makeAPause(MAKE_A_PAUSE_BEFORE_EACH_TEST);
        onView(withId(R.id.search_hashtag_edit__text_view))
                .perform(clearText());

        makeAPause(MAKE_A_PAUSE_BEFORE_EACH_TEST);

        onView(withId(R.id.search_hashtag_edit__text_view))
                .perform(typeText(SEARCH_FOR_HASH_TAG), closeSoftKeyboard());

        makeAPause(MAKE_A_PAUSE_BEFORE_EACH_TEST);
        onView(withId(R.id.btn_search_status_or_hashtag)).perform(click());

        makeAPause(MAKE_A_PAUSE_BEFORE_EACH_TEST);
    }


    /**
     * Checks for the Toast Message
     */
    @Test
    public void checkForToastMessage(){

        onView(withId(R.id.search_hashtag_edit__text_view))
                .perform(clearText());

        makeAPause(MAKE_A_PAUSE_BEFORE_EACH_TEST);

        onView(withText(INITIAL_TEXT_BUTTON)).perform(click());

        makeAPause(MAKE_A_PAUSE_BEFORE_EACH_TEST);

        onView(withText("Enter a #hashtag or Tweet")).inRoot(withDecorView(not(is(mActivityRule.getActivity()
                .getWindow().getDecorView())))).check(matches(isDisplayed()));

        makeAPause(MAKE_A_PAUSE_BEFORE_EACH_TEST);
    }



    /**
     * Checks that the EditText's hit is equals to: Search For #Hashtag
     * @throws Exception
     */
    @Test
    public void checksForEditTextHint() throws Exception{
        onView(withId(R.id.search_hashtag_edit__text_view))
                .check(matches(withHint("Search For #Hashtag")));
    }


    /**
     * Returns the resource's identifier id
     *
     * @param s resource Identifier
     * @return id of the resource
     */
    private static int getResourceId(String s) {
        Context targetContext = InstrumentationRegistry.getTargetContext();
        String packageName = targetContext.getPackageName();
        return targetContext.getResources().getIdentifier(s, "id", packageName);
    }

    /**
     * Make a brief pause between each test
     *
     * @param milliseconds number of milliseconds between tests
     */
    private void makeAPause(long milliseconds) {
        try {
            Thread.sleep(milliseconds);

        } catch (InterruptedException e) {

            e.printStackTrace();

        }
    }
}
