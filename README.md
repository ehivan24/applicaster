# Applicaster's Tech Test

## **Main Activity** ##

![main.png](https://bitbucket.org/repo/7EEXyLX/images/4009673551-main.png)

### **Max number of requests per call is set to 10**  ###

![main_screen.png](https://bitbucket.org/repo/7EEXyLX/images/2212797274-main_screen.png)

## **ListView containing the information from the Twitter API** ##

![main_screen_tweet.png](https://bitbucket.org/repo/7EEXyLX/images/4268416910-main_screen_tweet.png)

## **AutoComplete from the previous searches,
The searches hashtags or Tweets are saved on local Database.
If the hashtag was previously searched, then it wont be added to the local Database.** ##

![drop_down_menu.png](https://bitbucket.org/repo/7EEXyLX/images/1297363847-drop_down_menu.png)

## **The app has a SwipeRefreshLayout to call the Twitter API and fetch new Data when the layout is swiped down.**##

![swipe_down.png](https://bitbucket.org/repo/7EEXyLX/images/3266248494-swipe_down.png)

##** The Application has a Thread, Timer, that refreshes the app every 5 seconds, to observe this behavior I suggest to search for keywords that are updated often like 'cnn' or 'threading searches'
the application allows to search by #hashtag or tweets **##

![swipe_down.png]
(https://bitbucket.org/repo/7EEXyLX/images/1496688045-swipe_down.png)

##** To run the Unit Tests, applicaster/Applicaster/app/src/test/java/com/ada/ehivan/applicasterLauncherUnitTest.java, right click on the name of the class to run all the tests and select Run or to run an individual test right click on the name of the function and select Run **##

![Screen Shot 2017-04-30 at 9.59.57 PM.png](https://bitbucket.org/repo/7EEXyLX/images/2994304446-Screen%20Shot%202017-04-30%20at%209.59.57%20PM.png)

##**To Run all the UI Test, /Applicaster/app/src/androidTest/java/com/ada/ehivan/applicaster/LauncherTest.java, right click on the name of the class to run all the tests and select Run and look at the phone or emulator. 
For every test there will be a pause to observe the behavior.**##

![Screen Shot 2017-04-30 at 9.55.26 PM.png](https://bitbucket.org/repo/7EEXyLX/images/603221816-Screen%20Shot%202017-04-30%20at%209.55.26%20PM.png)